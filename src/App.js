import { Container } from "reactstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Todo from "./components/Todo";
import TodoForm from "./components/TodoForm";
import { Provider } from "react-redux";
import Store from "./store/todoStore";

function App() {
  return (
    <Provider store={Store}>
      <Container fluid>
        <Todo />
        <TodoForm />
      </Container>
    </Provider>
  );
}

export default App;
