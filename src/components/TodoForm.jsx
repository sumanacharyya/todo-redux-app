import React, { useState } from "react";

import {
  Form,
  FormGroup,
  Input,
  Button,
  InputGroup,
  InputGroupText,
} from "reactstrap";

import { v4 as uuidv4 } from "uuid";

import { connect } from "react-redux";
import { addTodo } from "../action/todo";

const TodoForm = ({ addTodo }) => {
  const [title, setTitle] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (title === "") {
      return alert("Please enter Todo");
    }

    const todo = {
      title,
      id: uuidv4(),
    };

    addTodo(todo);

    setTitle("");
  };

  return (
    <Form>
      <FormGroup>
        <InputGroup>
          <Input
            type="text"
            name="todo"
            id="todo"
            placeholder="Type your next Todo"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
          <InputGroupText>
            <Button color="primary" onClick={handleSubmit}>
              Add
            </Button>
          </InputGroupText>
        </InputGroup>
      </FormGroup>
    </Form>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
  addTodo: (todo) => {
    dispatch(addTodo(todo));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoForm);
