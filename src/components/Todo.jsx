import React from "react";
import { connect } from "react-redux";
import { removeTodo } from "../action/todo";
import { ListGroup, ListGroupItem } from "reactstrap";
import { FaCheckCircle } from "react-icons/fa";

const Todo = ({ todos, markComplete }) => {
  return (
    <ListGroup>
      {todos.map((todo) => (
        <ListGroupItem key={todo.id}>
          {todo.title}
          <span onClick={() => markComplete(todo.id)} className="float-end">
            <FaCheckCircle />
          </span>
        </ListGroupItem>
      ))}
    </ListGroup>
  );
};

const mapStateToProps = (state) => ({
  todos: state.TodoReducer,
});

const mapDispatchToProps = (dispatch) => ({
  markComplete: (id) => {
    dispatch(removeTodo(id));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Todo);
