import { createStore, combineReducers } from "redux";

import { TodoReducer } from "../reducer/todo";

const rootReducer = combineReducers({
  TodoReducer,
});

const Store = createStore(rootReducer);

export default Store;
